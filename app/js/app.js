'use strict';

/* App Module */

var productcatApp = angular.module('productcatApp', [
  'ngRoute',
  'productcatAnimations',

  'productcatControllers',
  'productcatFilters',
  'productcatServices'
]);

productcatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/retailers', {
        templateUrl: 'partials/retailer.html',
        controller: 'RetailerCtrl'
      }).
      when('/retailer/:retailerId', {
        templateUrl: 'partials/retailer.html',
        controller: 'RetailerCtrl'
      }).
      when('/product/:productId', {
        templateUrl: 'partials/product.html',
        controller: 'ProductDetailCtrl'
      }).
      otherwise({
        redirectTo: '/retailers'
      });
  }]);
