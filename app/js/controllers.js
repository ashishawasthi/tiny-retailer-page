'use strict';
function init() {
		console.log(' init');
  window.init();
}
/* Controllers */

var productcatControllers = angular.module('productcatControllers', ['ui.bootstrap']);

productcatControllers.controller('RetailerCtrl', ['$scope', '$window', 'Product',
  function($scope, $window, Product) {
    $scope.products = Product.query();
    $scope.orderProp = 'age';
				$scope.slideInterval = 1000;
				$scope.slides = [];
				$scope.retailer = {};
				$scope.retailer.json = "";
				$scope.slides.push({image: 'img/products/p1.jpg', text: 'Text 1'});
				$scope.slides.push({image: 'img/products/p2.jpg', text: 'Text 2'});
				$scope.slides.push({image: 'img/products/b1.jpg', text: 'Text 3'});
				$scope.slides.push({image: 'img/products/b2.jpg', text: 'Text 4'});
				$scope.slides.push({image: 'img/products/m1.jpg', text: 'Text 5'});
				$scope.ROOT = 'https://retailscanner.appspot.com/_ah/api';
				
				$scope.loadApi = function() {
						console.log(' scope.loadApi');
						gapi.client.load('sts', 'v2', function() {
								console.log('sts API loaded');
								$scope.loadRetailer();
						}, $scope.ROOT);
				};
				$scope.loadRetailer = function() {
						console.log(' scope.loadRetailer');
						var params = {'latitude':28.463,'longitude':77.0866,'radius':100000,'limit':10};
      console.log("searchRetailers params:"); console.log(params);
						gapi.client.sts.searchRetailers(params).execute(function(resp) {
								console.log('searchRetailers response: '); console.log(resp);
								$scope.retailer.json = JSON.stringify(resp.list[0]);
						}, $scope.ROOT);
				};
				$window.init = function() {
						console.log(' window.init');
						$scope.$apply($scope.loadApi);
				};
  }]);

productcatControllers.controller('ProductDetailCtrl', ['$scope', '$routeParams', 'Product',
  function($scope, $routeParams, Product) {
    $scope.product = Product.get({productId: $routeParams.productId}, function(product) {
      $scope.mainImageUrl = product.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);
