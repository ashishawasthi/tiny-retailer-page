'use strict';

/* jasmine specs for controllers go here */
describe('ProductCat controllers', function() {

  beforeEach(function(){
    this.addMatchers({
      toEqualData: function(expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('productcatApp'));
  beforeEach(module('productcatServices'));

  describe('RetailerCtrl', function(){
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('products/products.json').
          respond([{name: 'M'}, {name: 'M'}]);

      scope = $rootScope.$new();
      ctrl = $controller('RetailerCtrl', {$scope: scope});
    }));


    it('should create "products" model with 2 products fetched from xhr', function() {
      expect(scope.products).toEqualData([]);
      $httpBackend.flush();

      expect(scope.products).toEqualData(
          [{name: 'Nexus S'}, {name: 'Motorola DROID'}]);
    });


    it('should set the default value of orderProp model', function() {
      expect(scope.orderProp).toBe('age');
    });
  });


  describe('ProductDetailCtrl', function(){
    var scope, $httpBackend, ctrl,
        xyzProductData = function() {
          return {
            name: 'product xyz',
                images: ['image/url1.png', 'image/url2.png']
          }
        };


    beforeEach(inject(function(_$httpBackend_, $rootScope, $routeParams, $controller) {
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('products/xyz.json').respond(xyzProductData());

      $routeParams.productId = 'xyz';
      scope = $rootScope.$new();
      ctrl = $controller('ProductDetailCtrl', {$scope: scope});
    }));


    it('should fetch product detail', function() {
      expect(scope.product).toEqualData({});
      $httpBackend.flush();

      expect(scope.product).toEqualData(xyzProductData());
    });
  });
});
