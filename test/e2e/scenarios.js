'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('ProductCat App', function() {

  it('should redirect index.html to index.html#/retailers', function() {
    browser().navigateTo('app/index.html');
    expect(browser().location().url()).toBe('/retailers');
  });


  describe('retailers view', function() {

    beforeEach(function() {
      browser().navigateTo('app/index.html#/retailers');
    });


    it('should filter the product list as user types into the search box', function() {
      expect(repeater('.products li').count()).toBe(20);

      input('query').enter('nexus');
      expect(repeater('.products li').count()).toBe(1);

      input('query').enter('motorola');
      expect(repeater('.products li').count()).toBe(8);
    });


    it('should be possible to control product order via the drop down select box', function() {
      input('query').enter('tablet'); //let's narrow the dataset to make the test assertions shorter

      expect(repeater('.products li', 'Product List').column('product.name')).
          toEqual(["Motorola XOOM\u2122 with Wi-Fi",
                   "MOTOROLA XOOM\u2122"]);

      select('orderProp').option('Alphabetical');

      expect(repeater('.products li', 'Product List').column('product.name')).
          toEqual(["MOTOROLA XOOM\u2122",
                   "Motorola XOOM\u2122 with Wi-Fi"]);
    });


    it('should render product specific links', function() {
      input('query').enter('m');
      element('.products li a').click();
      expect(browser().location().url()).toBe('/products/m');
    });
  });


  describe('Product view', function() {

    beforeEach(function() {
      browser().navigateTo('app/index.html#/products/m');
    });


    it('should display nexus-s page', function() {
      expect(binding('product.name')).toBe('Nexus S');
    });


    it('should display the first product image as the main product image', function() {
      expect(element('img.product.active').attr('src')).toBe('img/products/m1.jpg');
    });


    it('should swap main image if a thumbnail image is clicked on', function() {
      element('.product-thumbs li:nth-child(2) img').click();
      expect(element('img.product.active').attr('src')).toBe('img/products/m2.jpg');

      element('.product-thumbs li:nth-child(1) img').click();
      expect(element('img.product.active').attr('src')).toBe('img/products/m1.jpg');
    });
  });
});
